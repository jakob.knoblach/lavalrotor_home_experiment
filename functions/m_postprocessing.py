"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple
from scipy.interpolate import interp1d

def get_vec_accel(x, y, z) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    
    vektorbetrag = []
    
    for i in range(4348):
        xvektor = x[i]
        yvektor = y[i]
        zvektor = z[i]

        
        betrag = np.sqrt(xvektor**2 + yvektor**2 + zvektor**2)
        vektorbetrag.append(betrag)
    return np.array(vektorbetrag)

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    interpolation_points = np.linspace(min(time), max(time), len(time))  

    interp_func = np.interp(interpolation_points, time, data)
   
    return interpolation_points, interp_func


def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    
    f_help = 1 / np.mean(np.diff(time)) 
    länge = len(x)  
    freq = np.fft.fftfreq(länge, d=1/f_help) 
    amplitude = np.abs(np.fft.fft(x)) / länge 
    return amplitude[:länge//2], freq[:länge//2]

   