import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_kopfhörer.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
acceleration_x = list()
acceleration_y = list()
acceleration_z = list()
timestamp = list()
probe_dictionary = {"1ee847be-fddd-6ee4-892a-68c4555b0981": {"acceleration_x": acceleration_x, "acceleration_y": acceleration_y, "acceleration_z": acceleration_z, "timestamp": timestamp}}

# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
import time
import board
import adafruit_adxl34x

i2c = board.I2C()  # uses board.SCL and board.SDA
accelerometer = adafruit_adxl34x.ADXL345(i2c)    
count = 0
while count <= measure_duration_in_s:
    
    acceleration_x.append(accelerometer.acceleration[0])
    acceleration_y.append(accelerometer.acceleration[1])
    acceleration_z.append(accelerometer.acceleration[2])
    timestamp.append(count)
    time.sleep(1/1000)
    count += 0.0046
print(probe_dictionary)
# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
dateipfad = path_h5_file
with h5py.File(dateipfad, 'w') as datei:
    gruppenname = 'Raw Data'
    
    gruppe = datei.create_group(gruppenname)
    
    gruppe1 = gruppe.create_group('1ee847be-fddd-6ee4-892a-68c4555b0981')
    
    untergruppe1 = gruppe1.create_group('acceleration x')
    untergruppe2 = gruppe1.create_group('acceleration y')
    untergruppe3 = gruppe1.create_group('acceleration z')
    untergruppe4 = gruppe1.create_group('timestamp')
    
    attribute = datei['Raw Data']['1ee847be-fddd-6ee4-892a-68c4555b0981']
    
    attribute['acceleration x'].attrs['unit'] = 'm/s^2'
    attribute['acceleration y'].attrs['unit'] = 'm/s^2'
    attribute['acceleration z'].attrs['unit'] = 'm/s^2'
    attribute['timestamp'].attrs['unit'] = 's'
    
    data_key = probe_dictionary['1ee847be-fddd-6ee4-892a-68c4555b0981']
    
    acceleration_x_value = data_key.get('acceleration_x')
    acceleration_y_value = data_key.get('acceleration_y')
    acceleration_z_value = data_key.get('acceleration_z')
    timestamp_value = data_key.get('timestamp')
    
    untergruppe1.create_dataset('X-Werte', data = acceleration_x_value)
    untergruppe2.create_dataset('Y-Werte', data = acceleration_y_value)
    untergruppe3.create_dataset('Z-Werte', data = acceleration_z_value)
    untergruppe4.create_dataset('Zeitwerte', data = timestamp_value)


# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
